#include "gm1.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

static gm1_u8_t Pixels0[] = {
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

static gm1_u8_t Pixels0Enc[] = {
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80,
    0x29, 0x80
};

typedef struct membuf
{
    size_t count;
    void* buffer;
} membuf_t;

static gm1_size_t writebuf(void* userdata, const gm1_u8_t* buffer, gm1_size_t count)
{
    membuf_t* buf = (membuf_t*)userdata;
    buf->buffer = realloc(buf->buffer, buf->count + count);
    assert(buf->buffer);
    memcpy((unsigned char*)buf->buffer + buf->count, buffer, count);
    buf->count += count;
    return count;
}

int test(gm1_u16_t key, void* src, size_t w, size_t h, size_t bpp, const void* dst, size_t dstsize, int learn)
{
    gm1_image_t img = {
        .Pixels = (gm1_u8_t*)src,
        .Width = w,
        .Height = h,
        .BytesPerPixel = bpp,
        .BytesPerRow = img.Width*bpp,
        .ColorKey = (gm1_u8_t*)&key,
    };

    membuf_t buf = {
        .count = 0,
        .buffer = 0
    };
    
    gm1_ostream_t stream = {
        .UserData = &buf,
        .Write = writebuf
    };

    gm1_status_t status = gm1_write_tgx(&img, &stream);
    if(gm1_error(status))
        printf("gm1_write_tgx = %s\n", gm1_errorstr(status));
    assert(gm1_error(status) == 0);

    if(!learn)
    {
        assert(buf.count == dstsize);
        assert(0 == memcmp(buf.buffer, dst, dstsize));
    }
    else
    {
        printf("Start dump\n");
        for(size_t i = 0; i < buf.count; ++i)
            printf("0x%02X,", ((unsigned char*)buf.buffer)[i]);
        printf("\nEnd dump\n");
    }
    return 0;
}

int main(int argc, char *argv[])
{
    test(0xffff, Pixels0, 10, 10, 2, Pixels0Enc, sizeof(Pixels0Enc), 0);
    return 0;
}
