#!/usr/bin/env bash

STRONGHOLDDIR=/home/reggies/crusader

TGX16_LIST="
anim_buildings2.gm1
anim_castle.gm1
anim_goods.gm1
anim_tunnelors_guild.gm1
assasin_rope.gm1
cracks.gm1
enemy_faces.gm1
face800-blank.gm1
floats.gm1
icons_front_end.gm1
icons_placeholders.gm1
interface_army.gm1
interface_buttons.gm1
interface_icons2.gm1
interface_icons3.gm1
interface_ruins.gm1
interface_slider_bar.gm1
mapedit_buttons.gm1
oil_dropped.gm1
rock_chips.gm1"

TGX8_LIST="
anim_armourer.gm1
anim_baker.gm1
anim_blacksmith.gm1
anim_boiled_oil.gm1
anim_brewer.gm1
anim_chopping_block.gm1
anim_crusader_flag.gm1
anim_dungeon.gm1
anim_farmer.gm1
anim_flags.gm1
anim_flag_small.gm1
anim_fletcher.gm1
anim_healer.gm1
anim_hunter.gm1
anim_inn.gm1
anim_iron_miner.gm1
anim_market.gm1
anim_maypole.gm1
anim_pitch_dugout.gm1
anim_poleturner.gm1
anim_quarry.gm1
anim_shields.gm1
anim_stables.gm1
anim_stake.gm1
anim_tanner.gm1
anim_tunnels.gm1
anim_windmill.gm1
anim_woodcutter.gm1
body_animal_burning_big.gm1
body_animal_burning_small.gm1
body_arab_assasin.gm1
body_arab_ballista.gm1
body_arab_grenadier.gm1
body_arab_shortbow.gm1
body_arab_slave.gm1
body_arab_slinger.gm1
body_arab_swordsman.gm1
body_archer.gm1
body_armourer.gm1
body_baker.gm1
body_ballista.gm1
body_battering_ram.gm1
body_bear.gm1
body_blacksmith.gm1
body_boy.gm1
body_brewer.gm1
body_camel.gm1
body_catapult.gm1
body_chicken_brown.gm1
body_chicken.gm1
body_cow.gm1
body_crossbowman.gm1
body_crow.gm1
body_deer.gm1
body_dog.gm1
body_drunkard.gm1
body_farmer.gm1
body_fighting_monk.gm1
body_fire_eater.gm1
body_fireman.gm1
body_fletcher.gm1
body_ghost.gm1
body_girl.gm1
body_healer.gm1
body_horse_archer.gm1
body_horse_archer_top.gm1
body_horse_trader.gm1
body_hunter.gm1
body_innkeeper.gm1
body_iron_miner.gm1
body_jester.gm1
body_juggler.gm1
body_knight.gm1
body_knight_top.gm1
body_ladder_bearer.gm1
body_lady.gm1
body_lion.gm1
body_lord.gm1
body_maceman.gm1
body_man_burning.gm1
body_mangonel.gm1
body_miller.gm1
body_missile_2.gm1
body_missile_cow.gm1
body_missile_fire.gm1
body_missile.gm1
body_mother.gm1
body_ox.gm1
body_peasant.gm1
body_pikeman.gm1
body_pitch_worker.gm1
body_poleturner.gm1
body_priest.gm1
body_rabbit.gm1
body_saladin.gm1
body_seagull.gm1
body_shield.gm1
body_siege_engineer.gm1
body_siege_tower.gm1
body_spearman.gm1
body_splash.gm1
body_stonemason.gm1
body_swordsman.gm1
body_tanner.gm1
body_tent.gm1
body_trader.gm1
body_trebutchet.gm1
body_tunnelor.gm1
body_wolf.gm1
body_woodcutter.gm1
tree_apple.gm1
tree_birch.gm1
tree_cactii.gm1
Tree_Chestnut.gm1
tree_oak.gm1
tree_pine.gm1
tree_shrub1.gm1
tree_shrub2.gm1"

TILE_LIST="
killing_pits.gm1
pitch_ditches.gm1
tile_buildings1.gm1
tile_buildings2.gm1
tile_burnt.gm1
tile_castle.gm1
tile_churches.gm1
tile_data.gm1
tile_farmland.gm1
tile_flatties.gm1
tile_goods.gm1
tile_land3.gm1
tile_land8.gm1
tile_land_and_stones.gm1
tile_land_macros.gm1
tile_rocks8.gm1
tile_ruins.gm1
tile_sea8.gm1
tile_sea_new_01.gm1
tile_workshops.gm1"

FONT_LIST="
font_slanted.gm1
font_stronghold_aa.gm1
font_stronghold.gm1"

BITMAP_LIST="
tile_chevrons.gm1
tile_cliffs.gm1
tile_rocks_chevrons.gm1
tile_walls.gm1"

TGX16_CONST_LIST="
anim_campaign_map_flags.gm1
anim_dancing_bear.gm1
anim_dog_cage.gm1
anim_drawbridge.gm1
anim_ducking_stool.gm1
anim_gallows.gm1
anim_gibbet.gm1
anim_heads.gm1
anim_killing_pits.gm1
anim_rack.gm1
anim_stocks.gm1
anim_whitecaps.gm1
army_units.gm1
blast3.gm1
body_brazier.gm1
body_disease.gm1
body_fire2.gm1
body_fire.gm1
body_gate.gm1
body_info.gm1
body_steam.gm1
cursors.gm1
float_pop_circ.gm1
floats_new.gm1
icons_front_end_builder.gm1
icons_front_end_combat.gm1
icons_front_end_economics.gm1
mini_cursors.gm1
scribe.gm1
skirmish_trail_icons.gm1
smoke-30x30.gm1"

BITMAP_OTHER_LIST="
tile_rivers.gm1"

GM1_LIST="
$TGX16_LIST
$TGX8_LIST
$TILE_LIST
$FONT_LIST
$TGX16_CONST_LIST
$BITMAP_LIST
$BITMAP_OTHER_LIST"

TEMP_DIR=crusader-temp

GM1=./gm1conv

! [ -d $TEMP_DIR ] && mkdir $TEMP_DIR

for FILENAME in ${GM1_LIST//\\n/ }; do
    ORIGINAL_GM1="$STRONGHOLDDIR/gm.backup/$FILENAME"
    GAME_GM1="$STRONGHOLDDIR/gm/$FILENAME"
    TEMP_GM1_DIR="$TEMP_DIR/$FILENAME"
    
    echo "$(date -u) extract $FILENAME"
    $GM1 -x "$ORIGINAL_GM1" "$TEMP_GM1_DIR"
    echo "$(date -u) create $FILENAME"
    $GM1 -c "$TEMP_GM1_DIR" "$GAME_GM1"
done
