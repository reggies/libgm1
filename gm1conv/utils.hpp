#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <gm1.h>

namespace
{
    struct SurfaceDeleter
    {
        void operator()(SDL_Surface* surface)
        {
            SDL_FreeSurface(surface);
        }
    };

    struct PaletteDeleter
    {
        void operator()(SDL_Palette* palette)
        {
            SDL_FreePalette(palette);
        }
    };

    struct PixelFormatDeleter
    {
        void operator()(SDL_PixelFormat* pixel_format)
        {
            SDL_FreeFormat(pixel_format);
        }
    };

    struct SdlSurfaceLock
    {
        explicit SdlSurfaceLock(SDL_Surface* surface) : m_Surface(surface)
        {
            if(SDL_MUSTLOCK(m_Surface))
                SDL_LockSurface(m_Surface);
        }
        ~SdlSurfaceLock()
        {
            if(SDL_MUSTLOCK(m_Surface))
                SDL_UnlockSurface(m_Surface);
        }
    private:
        SDL_Surface* m_Surface;
    };
}

#endif
