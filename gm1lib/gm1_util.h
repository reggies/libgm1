/*
  Copyright (c) 2016 Alexey Natalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef GM1_UTIL_H_
#define GM1_UTIL_H_

#include "gm1.h"
#include "gm1_endian.h"

static gm1_status_t rwstatus(gm1_size_t rw, gm1_size_t expected)
{
    return (rw == expected ? GM1_SUCCESS : GM1_EIO);
}

static inline gm1_size_t gm1_istream_read(gm1_istream_t* is, gm1_u8_t* buf, gm1_size_t len)
{
    return is->Read(is->UserData, buf, len);
}

static inline gm1_size_t gm1_ostream_write(gm1_ostream_t* os, const gm1_u8_t* buf, gm1_size_t len)
{
    return os->Write(os->UserData, buf, len);
}

static inline gm1_size_t put_le32(gm1_ostream_t* os, gm1_u32_t n)
{
    gm1_swap32le(n);
    return gm1_ostream_write(os, (gm1_u8_t*)&n, 4);
}

static inline gm1_size_t put_le16(gm1_ostream_t* os, gm1_u16_t n)
{
    gm1_swap16le(n);
    return gm1_ostream_write(os, (gm1_u8_t*)&n, 2);
}

static inline gm1_size_t put_byte(gm1_ostream_t* os, gm1_u8_t n)
{
    return gm1_ostream_write(os, (gm1_u8_t*)&n, 1);
}

static inline gm1_size_t get_le32(gm1_istream_t* is, gm1_u32_t* out)
{
    gm1_size_t bytes = gm1_istream_read(is, (gm1_u8_t*)out, 4);
    gm1_swap32le(*out);
    return bytes;
}

static inline gm1_size_t get_le16(gm1_istream_t* is, gm1_u16_t* out)
{
    gm1_size_t bytes = gm1_istream_read(is, (gm1_u8_t*)out, 2);
    gm1_swap16le(*out);
    return bytes;
}

static inline gm1_size_t get_byte(gm1_istream_t* is, gm1_u8_t* out)
{
    return gm1_istream_read(is, (gm1_u8_t*)out, 1);
}

static inline void gm1_set_default_color_key(gm1_image_props_t* props)
{
    /* TODO check these values.
       it should be something like 0x1f78 in rgb555 or
       0111100000011111
    */
    gm1_rgb_t color_key;
    color_key.Red = 240;
    color_key.Green = 0;
    color_key.Blue = 248;
    gm1_pack_rgb(&color_key, &props->ColorKey);
}

#endif
