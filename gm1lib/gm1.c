/*
  Copyright (c) 2016 Alexey Natalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "gm1.h"
#include "gm1_endian.h"
#include "gm1_util.h"

#include <assert.h>
#include <string.h>

#ifdef GM1_DEBUG
#include <stdio.h>
#endif

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define TILE_HEIGHT 16u
#define TILE_WIDTH 30u

/* Constants for Big-endian pixel color masks */
#define RED_SHIFT 10u
#define GREEN_SHIFT 5u
#define BLUE_SHIFT 0u
#define RED_BITS 5u
#define GREEN_BITS 5u
#define BLUE_BITS 5u

#define TILE_BYTE_COUNT 512

#define TGX_CHUNK_MAX_LENGTH 32

#define TGX_CHUNK_COPY 0
#define TGX_CHUNK_SKIP 1
#define TGX_CHUNK_REPEAT 2
#define TGX_CHUNK_EOL 4

/* Minimum number of pixels in repeat chunk. The original
   game use 3 as default value.
   1 < TGX_REPEAT_MIN <= TGX_CHUNK_MAX_LENGTH
   See also gm1_rle_line function implementation.
*/
#define TGX_REPEAT_MIN 3

/* The only flag for the gm1_write_tgx function. See
   gm1_rle_line for implementation notes.
*/
#define TGX_RLE_FLAG_SKIPS_BEFORE_EOL 1

/* This array defines a number of pixels per each tile line. */
static const gm1_size_t kTileColumns[TILE_HEIGHT] = {
    2, 6, 10, 14, 18, 22, 26, 30, 30, 26, 22, 18, 14, 10, 6, 2
};

/* Lookup table for 5 to 8 bit expansion */
static gm1_u8_t kLookup58[] = {
    0, 8, 16, 24, 32, 41, 49, 57, 65, 74, 82, 90, 98, 106, 115, 123, 131, 139, 148, 156, 164, 172, 180, 189, 197, 205, 213, 222, 230, 238, 246, 255
};

static inline int status_failed(gm1_status_t status)
{
    return status != GM1_SUCCESS;
}

static inline int status_succeed(gm1_status_t status)
{
    return status == GM1_SUCCESS;
}

static inline int expand_byte(int n, int shift, int bits)
{
    return kLookup58[(n >> shift) & ((1 << bits) - 1)];
}

static inline int pack_plane(int n, int shift, int bits)
{
    return ((n >> (8 - bits)) & ((1 << bits) - 1)) << shift;
}

static inline int pack_rgb555(int red, int green, int blue)
{
    return pack_plane(red, RED_SHIFT, RED_BITS) |
        pack_plane(green, GREEN_SHIFT, GREEN_BITS) |
        pack_plane(blue, BLUE_SHIFT, BLUE_BITS);
}

#ifdef GM1_DEBUG
static const char* tgx_chunk_to_str(gm1_u8_t chunk)
{
    switch(chunk)
    {
    case TGX_CHUNK_COPY: return "COPY";
    case TGX_CHUNK_REPEAT: return "REP";
    case TGX_CHUNK_SKIP: return "SKIP";
    case TGX_CHUNK_EOL: return "EOL";
    default: return "UNK";
    }
}
#endif

static inline gm1_status_t gm1_get_chunk(gm1_istream_t* is, gm1_u8_t* type, gm1_u8_t* size)
{
    gm1_u8_t byte;
    gm1_size_t bytec = get_byte(is, &byte);
    *type = ((byte >> 5) & 7);
    *size = ((byte & 31) + 1);

#ifdef GM1_DEBUG
    static int chunk_idx = 0;
    ++chunk_idx;
    printf("%05d %s %u\n", chunk_idx, tgx_chunk_to_str(*type), *size);
#endif

    return rwstatus(bytec, 1);
}

static inline int is_pixels_equal(const gm1_u8_t* lhs, const gm1_u8_t* rhs, gm1_size_t bpp)
{
    return memcmp(lhs, rhs, bpp) == 0;
}

static inline int is_pixel_opaque(const gm1_u8_t* src, const gm1_u8_t* color_key, gm1_size_t bpp)
{
    return (color_key != NULL) && !is_pixels_equal(src, color_key, bpp);
}

static inline int is_pixel_visible(const gm1_u8_t* pixel,
                                   const gm1_u8_t* tile_init, const gm1_u8_t* tile_last,
                                   const gm1_u8_t* color_key, gm1_size_t bpp)
{
    return (pixel >= tile_last || pixel < tile_init) &&
        is_pixel_opaque(pixel, color_key, bpp);
}

static inline gm1_size_t get_pixel_count(const gm1_u8_t* init, const gm1_u8_t* last, gm1_size_t bpp)
{
    assert(last >= init);
    return (last - init) / bpp;
}

static gm1_status_t gm1_put_chunk(gm1_ostream_t* os, gm1_u8_t chunk, gm1_size_t count)
{
    assert(count > 0);
    assert(count <= TGX_CHUNK_MAX_LENGTH);

#ifdef GM1_DEBUG
    static int chunk_idx = 0;
    ++chunk_idx;
    printf("%05d %s %zu\n", chunk_idx, tgx_chunk_to_str(chunk), count);
#endif

    gm1_u8_t byte = (((chunk & 7) << 5) | ((count - 1) & 31));
    gm1_size_t putc = gm1_ostream_write(os, &byte, sizeof(byte));
    return rwstatus(putc, sizeof(byte));
}

static gm1_status_t gm1_put_chunk_eol(gm1_ostream_t* os)
{
    return gm1_put_chunk(os, TGX_CHUNK_EOL, 1);
}

static gm1_status_t gm1_put_chunk_skip(gm1_ostream_t* os, gm1_size_t count)
{
    gm1_status_t status = GM1_SUCCESS;
    while(count > 0 && status_succeed(status))
    {
        gm1_size_t chunk_len = MIN(count, TGX_CHUNK_MAX_LENGTH);
        count -= chunk_len;
        status = gm1_put_chunk(os, TGX_CHUNK_SKIP, chunk_len);
    }
    return status;
}

static gm1_status_t gm1_put_chunk_repeat(gm1_ostream_t* os, const gm1_u8_t* src, gm1_size_t bpp, gm1_size_t count)
{
    gm1_status_t status = GM1_SUCCESS;

    while(count > 0 && status_succeed(status))
    {
        gm1_size_t chunk_len = MIN(count, TGX_CHUNK_MAX_LENGTH);
        count -= chunk_len;
        status = gm1_put_chunk(os, TGX_CHUNK_REPEAT, chunk_len);
        if(status_succeed(status))
        {
            gm1_size_t putc = gm1_ostream_write(os, src, bpp);
            src += bpp;
            status = rwstatus(putc, bpp);
        }
    }

    return status;
}

static gm1_status_t gm1_put_chunk_copy(gm1_ostream_t* os, const gm1_u8_t* src, gm1_size_t bpp, gm1_size_t count)
{
    gm1_status_t status = GM1_SUCCESS;

    while(count > 0 && status_succeed(status))
    {
        gm1_size_t chunk_len = MIN(count, TGX_CHUNK_MAX_LENGTH);
        count -= chunk_len;
        status = gm1_put_chunk(os, TGX_CHUNK_COPY, chunk_len);
        if(status_succeed(status))
        {
            gm1_size_t byte_count = chunk_len * bpp;
            gm1_size_t putc = gm1_ostream_write(os, src, byte_count);
            src += byte_count;
            status = rwstatus(putc, byte_count);
        }
    }

    return status;
}

static gm1_status_t gm1_unrle_line(gm1_istream_t* istream,
                                   gm1_size_t read_max,
                                   gm1_u8_t* dest_data,
                                   gm1_size_t dest_width,
                                   gm1_size_t bytes_per_pixel,
                                   gm1_u8_t* color_key,
                                   gm1_size_t* read_count)
{
    const gm1_u8_t* dest_last = dest_data + dest_width * bytes_per_pixel;

    gm1_u8_t chunk = TGX_CHUNK_COPY;
    gm1_u8_t size;
    gm1_size_t i;
    gm1_size_t bytes_read = 0;
    gm1_status_t status = GM1_SUCCESS;

    while((chunk != TGX_CHUNK_EOL) && (bytes_read < read_max) && status_succeed(status))
    {
        status = gm1_get_chunk(istream, &chunk, &size);
        if(status_failed(status))
        {
            continue;
        }
        ++bytes_read;

        switch(chunk) {
        case TGX_CHUNK_COPY:
            {
                gm1_size_t byte_count = size * bytes_per_pixel;
                if(dest_last < dest_data + byte_count)
                {
                    status = GM1_EOVERFLOW;
                    continue;
                }
                if(bytes_read + byte_count > read_max)
                {
                    status = GM1_EUNDERFLOW;
                    continue;
                }
                gm1_size_t readc = gm1_istream_read(istream, dest_data, byte_count);
                dest_data += byte_count;
                bytes_read += byte_count;
                status = rwstatus(byte_count, readc);
            }
            break;

        case TGX_CHUNK_REPEAT:
            {
                gm1_size_t byte_count = bytes_per_pixel;
                if(dest_last < dest_data + size * bytes_per_pixel)
                {
                    status = GM1_EOVERFLOW;
                    continue;
                }
                if(bytes_read + byte_count > read_max)
                {
                    status = GM1_EUNDERFLOW;
                    continue;
                }
                gm1_size_t readc = gm1_istream_read(istream, dest_data, byte_count);
                for(i = 1; i < size; ++i)
                {
                    memcpy(dest_data + i * bytes_per_pixel, dest_data, bytes_per_pixel);
                }
                dest_data += size * bytes_per_pixel;
                bytes_read += byte_count;
                status = rwstatus(readc, byte_count);
            }
            break;

        case TGX_CHUNK_SKIP:
            {
                if(dest_last < dest_data + size * bytes_per_pixel)
                {
                    status = GM1_EOVERFLOW;
                    continue;
                }
                dest_data += size * bytes_per_pixel;
            }
            break;

        case TGX_CHUNK_EOL:
            {
                if(size != 1)
                {
                    status = GM1_ETGXCHUNKLEN;
                    continue;
                }
            }
            break;

        default:
            status = GM1_ETGXCHUNK;
            continue;
        }
    }

    if(read_count)
    {
        *read_count = bytes_read;
    }

    return status;
}

static gm1_status_t gm1_rle_line(const gm1_u8_t* src,
                                 gm1_size_t width,
                                 gm1_size_t bpp,
                                 const gm1_u8_t* color_key,
                                 gm1_size_t tile_offset,
                                 gm1_size_t tile_width,
                                 int flags,
                                 gm1_ostream_t* ostream)
{
    /* Do you like pseudocode?

        PIXELS  = Array of pixels
        N       = Number of elements in PIXELS
        for X from 1 to N
            S=X
            I=Index of visible pixel either transparent or obstructed by the tile such as
                I >= X and PIXELS[I] is visible
                I is minimal among all possible I
            R=Index of non repeat pixel such as
                R >= I
                PIXELS[R] != PIXELS[R-1]
                R is minimal among all possible R
            A=Index of repeat or invisible pixel such as
                A >= R
                (PIXELS[A] is invisible) or (PIXELS[A] = PIXELS[A-1])
                A is minimal among all possible A
            if I != X then write skip chunk
            if R != I then write repeat chunk
            if A != R then write copy chunk
            X=A
    */

    const gm1_u8_t* src_init = src;
    const gm1_u8_t* src_last = src_init + width * bpp;
    const gm1_u8_t* skip_init = src;
    const gm1_u8_t* skip_last = src;
    const gm1_u8_t* tile_init = src + tile_offset * bpp;
    const gm1_u8_t* tile_last = tile_init + tile_width * bpp;

    gm1_status_t status = GM1_SUCCESS;

    while(src != src_last && status_succeed(status))
    {
        /* Try to find as many transparent pixels as we can.
         */
        skip_init = src;
        while((src != src_last) && (!is_pixel_visible(src, tile_init, tile_last, color_key, bpp)))
        {
            src += bpp;
        }
        skip_last = src;
        assert(src == src_last || is_pixel_visible(src, tile_init, tile_last, color_key, bpp));

        /* Try to find as many repeat pixels as we can.
         */
        while((src != src_last) &&
              (is_pixel_visible(src, tile_init, tile_last, color_key, bpp)) &&
              (is_pixels_equal(src, skip_last, bpp)))
        {
            src += bpp;
        }

        gm1_size_t repeat_count = get_pixel_count(skip_last, src, bpp);
        if(repeat_count >= TGX_REPEAT_MIN)
        {
            /* Handle special case when we have to backtrack repeat scanner in
               order to prevent too small repeat chunk.
            */
            gm1_size_t trailing_count = (repeat_count % TGX_CHUNK_MAX_LENGTH);
            if(trailing_count < TGX_REPEAT_MIN)
            {
                src -= bpp * trailing_count;
            }

            status = gm1_put_chunk_skip(ostream, get_pixel_count(skip_init, skip_last, bpp));
            skip_init = skip_last;

            if(status_succeed(status))
            {
                /* We can be sure that at least one repeat pixel will be written.
                   Since we already have nRepeatCount > kRepeatMin repeating pixels.
                 */
                assert(skip_last != src);
                status = gm1_put_chunk_repeat(ostream, skip_last, bpp, get_pixel_count(skip_last, src, bpp));
            }
        }
        else
        {
            /* Repeat scanner fails to find anything sufficient. This time we will
               seek for any pixels.
            */
            src = skip_last;
            repeat_count = 1;
            while((src != src_last) &&
                  (is_pixel_visible(src, tile_init, tile_last, color_key, bpp)) &&
                  (src == skip_last || !is_pixels_equal(src, src - bpp, bpp) || repeat_count + 1 < TGX_REPEAT_MIN))
            {
                if(src != skip_last && is_pixels_equal(src, src - bpp, bpp))
                {
                    ++repeat_count;
                }
                else
                {
                    repeat_count = 1;
                }
                assert(repeat_count < TGX_REPEAT_MIN);
                src += bpp;
            }
            if(src != src_last)
            {
                /* If we hit there then these both are true:
                   1) skipLast is first visible pixel
                   2) repeat scanner didn't succeed
                   No way the cursor will be on the first opaque pixels after the
                   copy scanner completes. We must gather at least one pixel.
                */
                assert(src != skip_last);

                if(is_pixel_visible(src, tile_init, tile_last, color_key, bpp))
                {
                    /* Scanner must have a break because
                       kRepeatMin repeat pixels have been occured
                    */
                    assert(is_pixels_equal(src, src - bpp, bpp));
                    assert(repeat_count + 1 >= TGX_REPEAT_MIN);
                    src -= bpp * repeat_count;
                }
                else
                {
                    /* range from skipLast to src contains at least one copy pixel */
                }
            }

            /* After copy scanner we must be either on the EOL
               or at the transparent pixel
               or at the sequence of kRepeatMin repeating pixels
            */
            assert(src == src_last || src != skip_last);

            /* Without the flag set we will not write SKIP chunks before end of line */
            if(src != skip_last || flags & TGX_RLE_FLAG_SKIPS_BEFORE_EOL)
            {
                status = gm1_put_chunk_skip(ostream, get_pixel_count(skip_init, skip_last, bpp));
                skip_init = skip_last;

                if(status_succeed(status))
                {
                    status = gm1_put_chunk_copy(ostream, skip_last, bpp, get_pixel_count(skip_last, src, bpp));
                }
            }
        }
    }

    if(status_succeed(status))
    {
        /* Normally we are not going to put 'skip' chunks before
           the end of line. But some gm1 classes (IIRC class=6) are so
           stupid that we just have to do this.
        */
        if(flags & TGX_RLE_FLAG_SKIPS_BEFORE_EOL)
        {
            status = gm1_put_chunk_skip(ostream, get_pixel_count(skip_init, skip_last, bpp));
        }
    }

    if(status_succeed(status))
    {
        status = gm1_put_chunk_eol(ostream);
    }

    return status;
}

static gm1_status_t gm1_rle_scanlines(const gm1_image_t* image, gm1_ostream_t* ostream, int flags)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_u8_t* scanline = image->Pixels;
    gm1_size_t i;

    for(i = 0; i < image->Height && status_succeed(status); ++i)
    {
        status = gm1_rle_line(scanline,
                              image->Width,
                              image->BytesPerPixel,
                              image->ColorKey,
                              0, 0,
                              flags,
                              ostream);
        scanline += image->BytesPerRow;
    }

    return status;
}

static gm1_status_t gm1_decode_tile(gm1_istream_t* istream, gm1_image_t* image)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_u8_t* scanline = image->Pixels;
    gm1_size_t i;

    for(i = 0; i < TILE_HEIGHT && status_succeed(status); ++i)
    {
        gm1_size_t bytec = kTileColumns[i] * image->BytesPerPixel;
        gm1_size_t readc = gm1_istream_read(
            istream,
            scanline + (TILE_WIDTH - kTileColumns[i]) * image->BytesPerPixel / 2,
            bytec);
        scanline += image->BytesPerRow;
        status = rwstatus(readc, bytec);
    }
    return status;
}

static gm1_status_t gm1_encode_tile(const gm1_image_t* image, gm1_ostream_t* ostream)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_u8_t* scanline = image->Pixels;
    gm1_size_t i;
    for(i = 0; i < image->Height && status_succeed(status); ++i)
    {
        gm1_size_t bytec = kTileColumns[i] * image->BytesPerPixel;
        gm1_size_t writec = gm1_ostream_write(
            ostream,
            scanline + (TILE_WIDTH - kTileColumns[i]) / 2 * image->BytesPerPixel,
            bytec);
        scanline += image->BytesPerRow;
        status = rwstatus(writec, bytec);
    }
    return status;
}

static gm1_status_t gm1_copy_box(const gm1_image_t* image, gm1_u32_t box_offset, gm1_u32_t tile_offset, gm1_ostream_t* ostream)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_size_t i;

    const gm1_size_t box_height = tile_offset + TILE_HEIGHT / 2;

    for(i = 0; i < box_height && status_succeed(status); ++i)
    {
        const gm1_u8_t* src = image->Pixels + i* image->BytesPerRow;
        gm1_size_t tile_init = 0;
        gm1_size_t tile_last = 0;

        if(image->ColorKey)
        {
            if(i >= tile_offset && i < box_height)
            {
                const gm1_size_t tile_width = kTileColumns[i - tile_offset];
                const gm1_size_t tile_left = (TILE_WIDTH - tile_width) / 2;
                const gm1_size_t tile_right = tile_left + tile_width;
                tile_init = tile_left - MIN(tile_left, box_offset);
                tile_last = MIN(tile_right - MIN(tile_right, box_offset), image->Width);
            }
        }

        status = gm1_rle_line(src,
                              image->Width,
                              image->BytesPerPixel,
                              image->ColorKey,
                              tile_init,
                              tile_last - tile_init,
                              TGX_RLE_FLAG_SKIPS_BEFORE_EOL,
                              ostream);
    }
    return status;
}

extern const char* gm1_errorstr(gm1_status_t status)
{
    switch(status) {
    case GM1_SUCCESS: return "success";
    case GM1_FAILURE: return "general failure";
    case GM1_EIO: return "io error";
    case GM1_EPARAM: return "invalid argument";
    case GM1_EENTRYCLASS: return "invalid entry class";
    case GM1_EUNDERFLOW: return "buffer underflow";
    case GM1_EOVERFLOW: return "buffer overflow";
    case GM1_ETGXCHUNK: return "bad tgx chunk";
    case GM1_ETGXCHUNKLEN: return "bad tgx chunk length";
    }
    return "null";
}

extern gm1_status_t gm1_get_image_props(gm1_u32_t entry_class, gm1_image_props_t* props)
{
    memset(props, 0, sizeof(gm1_image_props_t));

    switch(entry_class)
    {
    case GM1_ENTRY_CLASS_TILE:
    case GM1_ENTRY_CLASS_TGX16CONSTSIZE:
    case GM1_ENTRY_CLASS_TGX16:
    case GM1_ENTRY_CLASS_GLYPH:
        props->BytesPerPixel = 2;
        props->bHasColorKey = 1;
        gm1_set_default_color_key(props);
        return GM1_SUCCESS;

    case GM1_ENTRY_CLASS_TGX8:
        props->BytesPerPixel = 1;
        props->bHasColorKey = 1;
        /* Zero is an index in the color table */
        props->ColorKey = 0;
        return GM1_SUCCESS;

    case GM1_ENTRY_CLASS_ANOTHERBITMAP:
    case GM1_ENTRY_CLASS_BITMAP:
        props->BytesPerPixel = 2;
        props->bHasColorKey = 0;
        return GM1_SUCCESS;

    }

    return GM1_EENTRYCLASS;
}

extern gm1_status_t gm1_read_tile(gm1_istream_t* istream,
                                  gm1_size_t read_max,
                                  gm1_u32_t box_offset,
                                  gm1_u32_t box_width,
                                  gm1_u32_t tile_offset,
                                  gm1_image_t* image)
{
    if(read_max < TILE_BYTE_COUNT)
    {
        return GM1_EUNDERFLOW;
    }

    if(box_offset + box_width > image->Width ||
       tile_offset + TILE_HEIGHT > image->Height)
    {
        return GM1_EPARAM;
    }

    gm1_image_t tile;
    memset(&tile, 0, sizeof(gm1_image_t));
    tile.Width = TILE_WIDTH;
    tile.Height = TILE_HEIGHT;
    tile.BytesPerPixel = image->BytesPerPixel;
    tile.BytesPerRow = image->BytesPerRow;
    tile.Pixels = image->Pixels + tile_offset * image->BytesPerRow;

    gm1_status_t status = gm1_decode_tile(istream, &tile);
    if(status_failed(status))
    {
        return status;
    }

    if(read_max != TILE_BYTE_COUNT)
    {
        gm1_image_t box;
        memset(&box, 0, sizeof(gm1_image_t));
        box.Width = box_width;
        box.Height = image->Height;
        box.BytesPerPixel = image->BytesPerPixel;
        box.BytesPerRow = image->BytesPerRow;
        box.Pixels = image->Pixels + box_offset * image->BytesPerPixel;

        gm1_size_t unread_bytes = read_max - TILE_BYTE_COUNT;
        status = gm1_read_tgx(istream, unread_bytes, &box);
    }

    return status;
}

extern gm1_status_t gm1_read_bitmap(gm1_istream_t* istream, gm1_image_t* image)
{
    gm1_status_t status = GM1_SUCCESS;
    const gm1_size_t bytes_per_line = image->Width * image->BytesPerPixel;
    gm1_u8_t* scanline = image->Pixels;
    gm1_size_t i;

    for(i = 0; i < image->Height && status_succeed(status); ++i)
    {
        gm1_size_t readc = gm1_istream_read(istream, scanline, bytes_per_line);
        scanline += image->BytesPerRow;
        status = rwstatus(readc, bytes_per_line);
    }

    return status;
}

extern gm1_status_t gm1_read_header(gm1_istream_t* istream, gm1_header_t* header)
{
    gm1_size_t readc = 0;

    readc += get_le32(istream, &header->Unused1);
    readc += get_le32(istream, &header->Unused2);
    readc += get_le32(istream, &header->Unused3);
    readc += get_le32(istream, &header->EntryCount);
    readc += get_le32(istream, &header->Unused4);
    readc += get_le32(istream, &header->EntryClass);
    readc += get_le32(istream, &header->Unused5);
    readc += get_le32(istream, &header->Unused6);
    readc += get_le32(istream, &header->Category);
    readc += get_le32(istream, &header->Unused7);
    readc += get_le32(istream, &header->Unused8);
    readc += get_le32(istream, &header->Unused9);
    readc += get_le32(istream, &header->Width);
    readc += get_le32(istream, &header->Height);
    readc += get_le32(istream, &header->Unused10);
    readc += get_le32(istream, &header->Unused11);
    readc += get_le32(istream, &header->Unused12);
    readc += get_le32(istream, &header->Unused13);
    readc += get_le32(istream, &header->AnchorX);
    readc += get_le32(istream, &header->AnchorY);
    readc += get_le32(istream, &header->DataSize);
    readc += get_le32(istream, &header->Unused14);

    gm1_u16_t color_table[GM1_NUM_PALETTES][GM1_NUM_COLORS];
    readc += gm1_istream_read(istream, (gm1_u8_t*)color_table, sizeof(color_table));

#ifdef GM1_DEBUG
    printf("sizeof(gm1_rgb_t) = %d\n"
           "offsetof(gm1_rgb_t, Red) = %d\n"
           "offsetof(gm1_rgb_t, Green) = %d\n"
           "offsetof(gm1_rgb_t, Blue) = %d\n"
           (int)sizeof(gm1_rgb_t),
           (int)offsetof(gm1_rgb_t, Red),
           (int)offsetof(gm1_rgb_t, Green),
           (int)offsetof(gm1_rgb_t, Blue));
#endif

    for(gm1_u32_t i = 0; i < GM1_NUM_PALETTES; ++i)
    {
        for(gm1_u32_t j = 0; j < GM1_NUM_COLORS; ++j)
        {
            gm1_unpack_rgb(color_table[i][j], &(header->ColorTable[i][j]));
        }
    }

    gm1_status_t status = rwstatus(readc, 88 + GM1_NUM_COLORS * GM1_NUM_PALETTES * 2);

#ifdef GM1_DEBUG

    printf("gm1_read_header() : readc=%ld, expected=%d, status=%d\n", readc, GM1_NUM_COLORS * GM1_NUM_PALETTES * 2, status);

#endif

    return status;
}

extern gm1_status_t gm1_read_entries(gm1_istream_t* istream, gm1_u32_t entry_class, gm1_entry_t* entries, gm1_size_t entry_count)
{
    gm1_size_t readc = 0;
    gm1_size_t i;

    for(i = 0; i < entry_count; ++i)
    {
        readc += get_le32(istream, &entries[i].Offset);
    }

    for(i = 0; i < entry_count; ++i)
    {
        readc += get_le32(istream, &entries[i].Size);
    }

    for(i = 0; i < entry_count; ++i)
    {
        readc += get_le16(istream, &entries[i].Header.Width);
        readc += get_le16(istream, &entries[i].Header.Height);
        readc += get_le16(istream, &entries[i].Header.PosX);
        readc += get_le16(istream, &entries[i].Header.PosY);
        readc += get_byte(istream, &entries[i].Header.PartId);
        readc += get_byte(istream, &entries[i].Header.PartSize);
        readc += get_le16(istream, &entries[i].Header.TileOffset);
        readc += get_byte(istream, &entries[i].Header.Alignment);
        readc += get_byte(istream, &entries[i].Header.BoxOffset);
        readc += get_byte(istream, &entries[i].Header.BoxWidth);
        readc += get_byte(istream, &entries[i].Header.Flags);
    }

    gm1_status_t status = rwstatus(readc, entry_count * 24);
    if(gm1_error(status))
        return status;

    if(entry_class == GM1_ENTRY_CLASS_BITMAP ||
       entry_class == GM1_ENTRY_CLASS_ANOTHERBITMAP) {
        for(i = 0; i < entry_count; ++i)
        {
            if(entries[i].Header.Height < 7)
            {
                status = GM1_FAILURE;
            }
            else
            {
                entries[i].Header.Height -= 7;
            }
        }
    }

    return status;
}

extern gm1_status_t gm1_read_glyph(gm1_istream_t* istream, gm1_size_t read_max, gm1_image_t* image)
{
    /* TODO convert pixel format */
    return gm1_read_tgx(istream, read_max, image);
}

extern gm1_status_t gm1_write_glyph(const gm1_image_t* image, gm1_ostream_t* ostream)
{
    /* TODO convert pixel format */
    return gm1_rle_scanlines(image, ostream, TGX_RLE_FLAG_SKIPS_BEFORE_EOL);
}

extern gm1_status_t gm1_read_tgx(gm1_istream_t* istream, gm1_size_t read_max, gm1_image_t* image)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_size_t unread_bytes = read_max;
    gm1_size_t i;

    for(i = 0; i < image->Height && status_succeed(status) && unread_bytes > 0; ++i)
    {
        gm1_u8_t* dst = image->Pixels + i*image->BytesPerRow;
        gm1_size_t readc = 0;
        status = gm1_unrle_line(istream,
                                unread_bytes,
                                dst,
                                image->Width,
                                image->BytesPerPixel,
                                image->ColorKey,
                                &readc);

        if(readc > unread_bytes)
        {
            status = GM1_EUNDERFLOW;
        }
        else
        {
            unread_bytes -= readc;
        }
    }

    return status;
}

extern gm1_status_t gm1_write_tgx(const gm1_image_t* image, gm1_ostream_t* ostream)
{
    int flags = 0;
    if(image->BytesPerPixel > 1)
    {
        flags = TGX_RLE_FLAG_SKIPS_BEFORE_EOL;
    }
    return gm1_rle_scanlines(image, ostream, flags);
}

extern gm1_status_t gm1_write_tile(const gm1_image_t* image,
                                   gm1_u32_t box_offset,
                                   gm1_u32_t box_width,
                                   gm1_u32_t tile_offset,
                                   gm1_ostream_t* ostream)
{
    if(tile_offset + TILE_HEIGHT > image->Height ||
       box_offset + box_width > image->Width ||
       image->Width < TILE_WIDTH)
    {
        return GM1_EPARAM;
    }

    gm1_image_t tile;
    memset(&tile, 0, sizeof(gm1_image_t));
    tile.Width = TILE_WIDTH;
    tile.Height = TILE_HEIGHT;
    tile.BytesPerRow = image->BytesPerRow;
    tile.BytesPerPixel = image->BytesPerPixel;
    tile.Pixels = image->Pixels + image->BytesPerRow * tile_offset;
    tile.ColorKey = image->ColorKey;

    gm1_status_t status = gm1_encode_tile(&tile, ostream);
    if(status_failed(status))
    {
        return status;
    }

    if(box_width > 0)
    {
        gm1_image_t box;
        memset(&box, 0, sizeof(gm1_image_t));
        box.Width = box_width;
        box.Height = image->Height;
        box.BytesPerRow = image->BytesPerRow;
        box.BytesPerPixel = image->BytesPerPixel;
        box.Pixels = image->Pixels + image->BytesPerPixel * box_offset;
        box.ColorKey = image->ColorKey;
        return gm1_copy_box(&box, box_offset, tile_offset, ostream);
    }

    return status;
}

extern gm1_status_t gm1_write_bitmap(const gm1_image_t* image, gm1_ostream_t* ostream)
{
    gm1_status_t status = GM1_SUCCESS;
    gm1_size_t bytes_per_line = image->Width * image->BytesPerPixel;
    gm1_size_t i;

    for(i = 0; i < image->Height && status_succeed(status); ++i)
    {
        const gm1_u8_t* src = image->Pixels + i*image->BytesPerRow;
        gm1_size_t putc = gm1_ostream_write(ostream, src, bytes_per_line);
        status = rwstatus(putc, bytes_per_line);
    }
    return status;
}

extern gm1_status_t gm1_write_header(gm1_ostream_t* ostream, const gm1_header_t* header)
{
    gm1_size_t putc = 0;

    putc += put_le32(ostream, header->Unused1);
    putc += put_le32(ostream, header->Unused2);
    putc += put_le32(ostream, header->Unused3);
    putc += put_le32(ostream, header->EntryCount);
    putc += put_le32(ostream, header->Unused4);
    putc += put_le32(ostream, header->EntryClass);
    putc += put_le32(ostream, header->Unused5);
    putc += put_le32(ostream, header->Unused6);
    putc += put_le32(ostream, header->Category);
    putc += put_le32(ostream, header->Unused7);
    putc += put_le32(ostream, header->Unused8);
    putc += put_le32(ostream, header->Unused9);
    putc += put_le32(ostream, header->Width);
    putc += put_le32(ostream, header->Height);
    putc += put_le32(ostream, header->Unused10);
    putc += put_le32(ostream, header->Unused11);
    putc += put_le32(ostream, header->Unused12);
    putc += put_le32(ostream, header->Unused13);
    putc += put_le32(ostream, header->AnchorX);
    putc += put_le32(ostream, header->AnchorY);
    putc += put_le32(ostream, header->DataSize);
    putc += put_le32(ostream, header->Unused14);

    gm1_u16_t color_table[GM1_NUM_PALETTES][GM1_NUM_COLORS];
    for(gm1_u32_t i = 0; i < GM1_NUM_PALETTES; ++i)
    {
        for(gm1_u32_t j = 0; j < GM1_NUM_COLORS; ++j)
        {
            color_table[i][j] = pack_rgb555(header->ColorTable[i][j].Red,
                                            header->ColorTable[i][j].Green,
                                            header->ColorTable[i][j].Blue);
        }
    }

    putc += gm1_ostream_write(ostream, (gm1_u8_t*)color_table, sizeof(color_table));

    return rwstatus(putc, 88 + sizeof(color_table));
}

extern gm1_status_t gm1_write_entries(gm1_ostream_t* ostream, gm1_u32_t entry_class, const gm1_entry_t* entries, gm1_size_t entry_count)
{
    gm1_size_t putc = 0;
    gm1_size_t i;

    for(i = 0; i < entry_count; ++i)
    {
        putc += put_le32(ostream, entries[i].Offset);
    }

    for(i = 0; i < entry_count; ++i)
    {
        putc += put_le32(ostream, entries[i].Size);
    }

    for(i = 0; i < entry_count; ++i)
    {
        gm1_entry_header_t entry = entries[i].Header;
        if(entry_class == GM1_ENTRY_CLASS_BITMAP ||
           entry_class == GM1_ENTRY_CLASS_ANOTHERBITMAP)
        {
            entry.Height += 7;
        }
        putc += put_le16(ostream, entry.Width);
        putc += put_le16(ostream, entry.Height);
        putc += put_le16(ostream, entry.PosX);
        putc += put_le16(ostream, entry.PosY);
        putc += put_byte(ostream, entry.PartId);
        putc += put_byte(ostream, entry.PartSize);
        putc += put_le16(ostream, entry.TileOffset);
        putc += put_byte(ostream, entry.Alignment);
        putc += put_byte(ostream, entry.BoxOffset);
        putc += put_byte(ostream, entry.BoxWidth);
        putc += put_byte(ostream, entry.Flags);
    }

    return rwstatus(putc, entry_count * 24);
}

extern gm1_status_t gm1_read_image(
    gm1_istream_t* istream,
    gm1_size_t read_max,
    gm1_u32_t entry_class,
    const gm1_entry_header_t* entry_header,
    gm1_image_t* image)
{
    switch(entry_class)
    {
    case GM1_ENTRY_CLASS_TGX8:
    case GM1_ENTRY_CLASS_TGX16:
    case GM1_ENTRY_CLASS_TGX16CONSTSIZE:
        return gm1_read_tgx(istream, read_max, image);

    case GM1_ENTRY_CLASS_GLYPH:
        return gm1_read_glyph(istream, read_max, image);

    case GM1_ENTRY_CLASS_TILE:
        return gm1_read_tile(istream, read_max, entry_header->BoxOffset,
                             entry_header->BoxWidth, entry_header->TileOffset, image);

    case GM1_ENTRY_CLASS_BITMAP:
    case GM1_ENTRY_CLASS_ANOTHERBITMAP:
        return gm1_read_bitmap(istream, image);
    }

    return GM1_EENTRYCLASS;
}

extern gm1_status_t gm1_write_image(
    const gm1_image_t* image,
    gm1_u32_t entry_class,
    const gm1_entry_header_t* entry_header,
    gm1_ostream_t* ostream)
{
    switch(entry_class)
    {
    case GM1_ENTRY_CLASS_TGX8:
    case GM1_ENTRY_CLASS_TGX16:
    case GM1_ENTRY_CLASS_TGX16CONSTSIZE:
        return gm1_write_tgx(image, ostream);

    case GM1_ENTRY_CLASS_GLYPH:
        return gm1_write_glyph(image, ostream);

    case GM1_ENTRY_CLASS_TILE:
        return gm1_write_tile(image, entry_header->BoxOffset, entry_header->BoxWidth, entry_header->TileOffset, ostream);

    case GM1_ENTRY_CLASS_BITMAP:
    case GM1_ENTRY_CLASS_ANOTHERBITMAP:
        return gm1_write_bitmap(image, ostream);
    }

    return GM1_EENTRYCLASS;
}

extern void gm1_pack_rgb(const gm1_rgb_t* rgb, gm1_u16_t* pixel)
{
    *pixel = pack_rgb555(rgb->Red, rgb->Green, rgb->Blue);
}

extern void gm1_unpack_rgb(gm1_u16_t pixel, gm1_rgb_t* rgb)
{
    rgb->Red = expand_byte(pixel, RED_SHIFT, RED_BITS);
    rgb->Green = expand_byte(pixel, GREEN_SHIFT, GREEN_BITS);
    rgb->Blue = expand_byte(pixel, BLUE_SHIFT, BLUE_BITS);
}

extern int gm1_error(gm1_status_t status)
{
    return status != GM1_SUCCESS;
}
